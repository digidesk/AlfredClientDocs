.. _GettingStarted:

Getting Started with Alfred
===========================

Setting up MATLAB for AlfredClient
----------------------------------

The first thing that you'll need to do in order to set up AlfredClient is
determine the address of the desired AlfredServer [1]_. In the case of the
Digital Quantum Matter Laboratory, the address of the primary AlfredServer
instance at the time of writing is ``ws://dqmlab.iqc.uwaterloo.ca:30000``.

Now, make an instance of the AlfredClient object by executing the AlfredClient
class's constructor (see :ref:`AlfredClientDocs`)::

   ac = alfredClient('ws://InsertServerAddress.com')

At this time,

.. _cookbook:

AlfredClient Cookbook
---------------------


.. [#] Each AlfredServer instance is responsible for accepting Alfred jobs, coordinating instrument usage, and storing and retrieving Alfred job data. So, knowing where to find the AlfredServer that you need is very important.

