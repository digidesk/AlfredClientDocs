.. Alfred Client documentation master file, created by
   sphinx-quickstart on Fri Jan 08 18:03:00 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

AlfredClient
=============

Welcome to the landing page for AlfredClient documentation. The AlfredClient, as
of the time of writing, refers to the particular MATLAB implementation of the
AlfredServer client. This client is responsible for interfacing with the
corresponding AlfredServer implementation of the Alfred server. Both of these
projects are authored primarily by |Jeremy| Bejanin with feedback and assistance
provided by the remainder of the Digital Quantum Matter Laboratory.

Any reference to AlfredClient herein is only a reference to that implementation
written by |Jeremy| Bejanin. There exists, to date, no formal specification for
AlfredClient (or AlfredServer) that would allow for alternative implementations.
Such a specification would be considered welcome should any individual desire to
make such a contribution to this open-source project.

.. contents:: Landing Page Contents

Features
--------
AlfredClient is only useful in the context of a corresponding Alfred
Server. Using the client you can:

   * Retrieve data from past experiments
   * Send custom jobs that the Alfred Server will execute
   * Retrieve dilution refrigerator temperature information

.. For more information regarding the particular methods available to the client,
.. please see the :ref:`Alfred Client Docs <alfredClientDocs>` or the :ref:`Cookbook <cookbook>`.

Installation
------------
AlfredClient is managed using Git_ [1]_. Thus, the first thing you will need to
do is clone the AlfredClient repository. As of the time of this writing
AlfredClient is stored on Gitlab.com. To clone the repository with Git use your
favorite Git interface with the following url or execute the following Git
command in the shell:

.. code-block:: bash
   :linenos:

   git clone git@gitlab.com:DQMLab/AlfredClient.git

If successful, AlfredClient has now been installed to the directory in which
:code:`git clone` was called. To get started with AlfredClient please refer to
:ref:`GettingStarted`.

Contribute
----------

- Issue Tracker: https://gitlab.com/DQMLab/AlfredClient/issues
- Source Code: git@gitlab.com:DQMLab/AlfredClient.git

Support
-------

If you are having issues, please let us know. The best way to get your issue
resolved is to post an issue on the Issue Tracker or to concact the lead
developer |Jeremy| Bejanin by `email <jeremy.bejanin@gmail.com>`_.

License
-------

.. error:: The license for this project has yet to be determined.

.. Contents:
.. toctree::
   :hidden:
   :glob:
   :maxdepth: 2

   intro/*
..   alfred/*

.. Indices and tables
.. ==================
..
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

.. Common substitutions below
.. |Jeremy| unicode:: J 0xe9 r 0xe9 my
.. Hyperlinks below
.. _Git: https://git-scm.com/
.. Footnotes below
.. [#] A great introduction to using Git can be found at the start of the book `Pro Git <https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control>`_ written by Scott Chacon.
